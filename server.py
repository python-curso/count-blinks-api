# -*- coding: utf-8 -*-
import os
from flask import Flask, request, jsonify, make_response
from flask_restful import Resource, Api
from json import dumps
from werkzeug.utils import secure_filename
from flask_cors import CORS, cross_origin

from scipy.spatial import distance as dist
from imutils.video import FileVideoStream
from imutils import face_utils
import imutils
import time
import dlib
import cv2

UPLOAD_FOLDER = 'folder'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'mp4'])

app = Flask(__name__)
CORS(app)
api = Api(app)
app.config['CORS_HEADERS'] = 'Content-Type'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 5024 * 5024


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def get_ear(eye):
    A = dist.euclidean(eye[1], eye[5])
    B = dist.euclidean(eye[2], eye[4])

    C = dist.euclidean(eye[0], eye[3])
    ear = (A + B) / (2.0 * C)
    return ear


class UploadFiles(Resource):
    def post(self):
        if 'file' not in request.files:
            return make_response(jsonify({"message": "Arquivo obrigatório!"}), 500)
        file = request.files['file']

        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

        COUNTER = 0
        TOTAL = 0
        if file and allowed_file(file.filename):
            EYE_AR_THRESH = 0.23
            EYE_AR_CONSEC_FRAMES = 3

            detector = dlib.get_frontal_face_detector()
            predictor = dlib.shape_predictor(
                'shape_predictor_68_face_landmarks.dat')

            (lStart, lEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
            (rStart, rEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]

            fileVideo = FileVideoStream(
                app.config['UPLOAD_FOLDER'] + "/" + filename).start()

            while True:
                frame = fileVideo.read()
                if frame is None:
                    break
                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

                rects = detector(gray, 0)

                for rect in rects:
                    shape = predictor(gray, rect)
                    shape = face_utils.shape_to_np(shape)

                    leftEye = shape[lStart:lEnd]
                    rightEye = shape[rStart:rEnd]
                    leftEAR = get_ear(leftEye)
                    rightEAR = get_ear(rightEye)

                    ear = (leftEAR + rightEAR) / 2.0

                    if ear < EYE_AR_THRESH:
                        COUNTER += 1
                    else:
                        if COUNTER >= EYE_AR_CONSEC_FRAMES:
                            TOTAL += 1

                        COUNTER = 0
        fileVideo.stop()
        return {"Quantidade de piscadas": TOTAL}


api.add_resource(UploadFiles, '/')

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000, debug=True)
